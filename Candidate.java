/**
 * @author Louisa Lin
 */
public class Candidate extends People{
    private int partyId;
    public int getPartyId(){
        return this.partyId;
    }

    public Candidate(int id, String name, String role, String regionId, int partyId){
        super(id, name, role, regionId);
        this.partyId = partyId;
    }
}
