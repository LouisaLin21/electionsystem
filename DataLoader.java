import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * @author Qiuning Liang
 * done with the help of a tutor
 */
public class DataLoader implements Loader{

	@Override
	public List<People> loadPeoplesFromFile(String filename) throws IOException {
		Path path = Paths.get(filename);
		List<String> lines = Files.readAllLines(path);
		ArrayList<People> peoples = new ArrayList<People>();
		String className = filename.split("\\.")[0];
		if (Voter.class.getSimpleName().equals(className)) {
			for (String line : lines) {
				String[] pieces = line.split(",");
				Voter voter = new Voter(
						Integer.parseInt(pieces[0]),
						pieces[1],
						pieces[2],
						pieces[3]);
				peoples.add(voter);
			}
		}
		if (Candidate.class.getSimpleName().equals(className)) {
			for (String line : lines) {
				String[] pieces = line.split(",");
				Candidate candidate = new Candidate(
						Integer.parseInt(pieces[0]),
						pieces[1],
						pieces[2],
						pieces[3],
						Integer.parseInt(pieces[4]));
				peoples.add(candidate);
			}
		}
		return peoples;
	}

	public List<Party> loadPartiesFromFile(String filename) throws IOException {
		Path path = Paths.get(filename);
		List<String> lines = Files.readAllLines(path);
		ArrayList<Party> parties = new ArrayList<Party>();
		for (String line : lines) {
			String[] pieces = line.split(",");
			Party party = new Party(pieces[0], pieces[1], pieces[2]);
			parties.add(party);
		}
		return parties;
	}

	public List<CastVote> loadCastVotesFromFile(String filename) throws IOException {
		Path path = Paths.get(filename);
		List<String> lines = Files.readAllLines(path);
		ArrayList<CastVote> castVotes = new ArrayList<CastVote>();
		for (String line : lines) {
			String[] pieces = line.split(",");
			CastVote castVote = new CastVote(Integer.parseInt(pieces[0]), pieces[1]);
			castVotes.add(castVote);
		}
		return castVotes;
	}

	public List<RegionSeat> loadRegionSeatsFromFile(String filename) throws IOException {
		Path path = Paths.get(filename);
		List<String> lines = Files.readAllLines(path);
		ArrayList<RegionSeat> regionSeats = new ArrayList<RegionSeat>();
		for (String line : lines) {
			String[] pieces = line.split(",");
			RegionSeat regionSeat = new RegionSeat(pieces[0], pieces[1], Integer.parseInt(pieces[2]));
			regionSeats.add(regionSeat);
		}
		return regionSeats;
	}

}
