import java.io.IOException;
import java.util.*;

/**
 * @author Louisa Lin and Qiuning Liang
 */
public class CalculationByParty implements Calculation {
    private static final int TOTAL_SEATS = 100;

    @Override
    public String voteCount(List<CastVote> castVotes) throws IOException {
        List<Party> partyList = new DataLoader().loadPartiesFromFile("Party.txt");
        int[] voteAmountByParty = new int[partyList.size()];
        // start counting
        for (CastVote castVote: castVotes) {
            for (int i = 0; i < partyList.size(); i++) {
                if(partyList.get(i).getId().equals(castVote.getChoiceId())) {
                    voteAmountByParty[i] = voteAmountByParty[i] + 1;
                }
            }
        }
        // finish counting
        // start comparing
        int maxIndex = 0;
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < voteAmountByParty.length; i++) {
            if(voteAmountByParty[maxIndex] < voteAmountByParty[i]) {
                maxIndex = i;
            }
        }
        // finish comparing
        // generate result
        stringBuilder.append("The voting results are as follows:\r\n");
        stringBuilder.append("WinningParty: " + partyList.get(maxIndex).toString()
                + ", Number of seats: " + (int)((double)(voteAmountByParty[maxIndex])/castVotes.size()*TOTAL_SEATS) + "\r\n\r\n");
        for (int i = 0; i < partyList.size(); i++) {
            if (i != maxIndex) {
                stringBuilder.append("OtherParty: " + partyList.get(i).toString()
                        + ", Number of seats: " + (int) ((double)(voteAmountByParty[i])/castVotes.size()*TOTAL_SEATS) + "\r\n");
            }
        }
        return stringBuilder.toString();
    }


}



