import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class GUIApplication extends Application {
    ArrayList<CastVote> voting;

    List<CastVote> castVotes;

    public static void main(String[] args) {
        Application.launch(args);
    }

    public GUIApplication() {
        voting = new ArrayList<>();
        try {
            castVotes = new DataLoader().loadCastVotesFromFile("CastVote.txt");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void start(Stage stage) throws Exception {
        Group root = new Group();

        // scene is associated with container, dimensions
        Scene scene = new Scene(root, 750, 300);
        scene.setFill(Color.BLACK);

        stage.setTitle("Rock Paper Scissors");
        stage.setScene(scene);

        Button partyButton = new Button("Calculation By Party");
        Button regionButton = new Button("Calculation By Region");
        Button exitButton = new Button("Exit");
        HBox buttons = new HBox();
        HBox textFields = new HBox();
        TextArea result = new TextArea("Welcome!");
        result.setPrefWidth(750);
        result.setEditable(false);

        VBox vbox = new VBox();
        vbox.getChildren().addAll(buttons, textFields);
        textFields.getChildren().add(result);
        buttons.getChildren().addAll(partyButton, regionButton, exitButton);
        partyButton.setOnAction(new Choice(result, 1));
        regionButton.setOnAction(new Choice(result, 2));
        exitButton.setOnAction(new Choice(result, 99));

        root.getChildren().add(vbox);

        stage.show();
    }

    class Choice implements EventHandler<ActionEvent> {
        private TextArea result;
        private int choice;

        public Choice(TextArea result, int choice) {
            this.result = result;
            this.choice = choice;
        }

        @Override
        public void handle(ActionEvent arg0) {
            String voteResult = "";
            try {
                if (choice == 1) {
                    CalculationByParty calculationByP = new CalculationByParty();

                    voteResult = calculationByP.voteCount(castVotes);

                } else if (choice == 2) {
                    CalculationByRegion calculationByRegion = new CalculationByRegion();

                    voteResult = calculationByRegion.voteCount(castVotes);

                } else if (choice == 99) {
                    System.exit(0);
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            this.result.setText(voteResult);
            ;
        }

    }
}
