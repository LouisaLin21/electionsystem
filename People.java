/**
 * @author Louisa Lin
 */
public class People{
    private int id;
    private String name;
    private String role;
    private String regionId;
    
    /**
     * get methods and constructor
     *  */ 
    public People(int id, String name, String role, String regionId) {
        this.id=id;
        this.name=name;
        this.role=role;
        this.regionId=regionId;
    }
    public int getId(){
        return this.id;
    }
    public String getName(){
        return this.name;
    }
    public String getRole(){
        return this.role;
    }
    public String getRegionId() {
        return this.regionId;
    }
}
 
