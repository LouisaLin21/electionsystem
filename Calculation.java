/**
 *  This is an interface for calculationByRegion and calculationbyparty
 *  @author  Louisa Lin
 */
import java.io.IOException;
import java.util.*;

public interface Calculation{
    String voteCount(List<CastVote> Votes) throws IOException;
}