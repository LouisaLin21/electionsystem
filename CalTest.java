import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;
import java.io.IOException;
import java.util.*;

/**
 * This class will be the unit test
 * @author  Louisa Lin
 */
public class CalTest {

    @Test
    public void testLoadVoters() throws IOException {
        DataLoader dataLoader = new DataLoader();
        List<People> peopleList = dataLoader.loadPeoplesFromFile("Voter.txt");
        System.out.println(peopleList);
    }

    @Test
    public void testLoadCandidates() throws IOException {
        DataLoader dataLoader = new DataLoader();
        List<People> peopleList = dataLoader.loadPeoplesFromFile("Candidate.txt");
        System.out.println(peopleList);
    }

    @Test
    public void testLoadParties() throws IOException {
        DataLoader dataLoader = new DataLoader();
        List<Party> parties = dataLoader.loadPartiesFromFile("Party.txt");
        System.out.println(parties);
    }

    @Test
    public void testLoadCastVotes() throws IOException {
        DataLoader dataLoader = new DataLoader();
        List<CastVote> castVotes = dataLoader.loadCastVotesFromFile("CastVote.txt");
        System.out.println(castVotes);
    }
}

