import java.io.IOException;
import java.util.*;

/**
 * @author  Louisa Lin and Qiuning Liang
 */
public class CalculationByRegion implements Calculation{
    @Override
    public String voteCount(List<CastVote> castVotes) throws IOException {
        DataLoader dataLoader = new DataLoader();
        List<RegionSeat> regionSeatList = dataLoader.loadRegionSeatsFromFile("RegionSeat.txt");
        List<People> peopleList = dataLoader.loadPeoplesFromFile("Voter.txt");
        List<Party> partyList = dataLoader.loadPartiesFromFile("Party.txt");
        int[][] voteAmountByRegionAndParty = new int[regionSeatList.size()][partyList.size()];
        // start counting
        for (CastVote castVote: castVotes) {
            String voterRegion = "";
            for (People people: peopleList) {
                if (people.getId() == castVote.getVoterId()) {
                    voterRegion = people.getRegionId();
                    break;
                }
            }
            for (int i = 0; i < regionSeatList.size(); i++) {
                for (int j = 0; j < partyList.size(); j++) {
                    if(regionSeatList.get(i).getId().equals(voterRegion) && partyList.get(j).getId().equals(castVote.getChoiceId())) {
                        voteAmountByRegionAndParty[i][j] = voteAmountByRegionAndParty[i][j] + 1;
                    }
                }
            }
        }
        int[] regionWinningIndexes = new int[regionSeatList.size()];
        for (int i = 0; i < voteAmountByRegionAndParty.length; i++) {
            int maxIndex = 0;
            for (int j = 0; j < partyList.size(); j++) {
                if(voteAmountByRegionAndParty[i][maxIndex] < voteAmountByRegionAndParty[i][j]) {
                    maxIndex = j;
                }
            }
            regionWinningIndexes[i] = maxIndex;
        }

        int[] seatAmountByParty = new int[partyList.size()];
        for (int i = 0; i < partyList.size(); i++) {
            for (int j = 0; j < regionWinningIndexes.length; j++) {
                if (regionWinningIndexes[j] == i) {
                    seatAmountByParty[i] = seatAmountByParty[i] + regionSeatList.get(j).getSeatAmount();
                }
            }
        }
        // finish counting
        // start comparing
        int maxIndex = 0;
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < seatAmountByParty.length; i++) {
            if(seatAmountByParty[maxIndex] < seatAmountByParty[i]) {
                maxIndex = i;
            }
        }
        // finish comparing
        // generate result
        stringBuilder.append("The voting results are as follows:\r\n");
        stringBuilder.append("WinningParty: " + partyList.get(maxIndex).toString()
                + ", Number of seats: " + seatAmountByParty[maxIndex] + "\r\n\r\n");
        for (int i = 0; i < partyList.size(); i++) {
            if (i != maxIndex) {
                stringBuilder.append("OtherParty: " + partyList.get(i).toString()
                        + ", Number of seats: " + seatAmountByParty[i] + "\r\n");
            }
        }
        return stringBuilder.toString();
    }
}
