/**
 * @author Louisa Lin
 */
public class CastVote {
    //This class would record the id of the person and the candiate they are voting for.
    //An object of the two types to load in Application class
    private int voterId;
    private String choiceId;
    public CastVote(int voterId, String choiceId){
        this.voterId=voterId;
        this.choiceId=choiceId;
    }

    /**
     * Get methods
     * @return
     */
    public int getVoterId(){
        return this.voterId;
    }
    public String getChoiceId(){
        return this.choiceId;
    }
}

