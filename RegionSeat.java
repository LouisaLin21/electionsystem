/**
 * @author Qiuning Liang
 */
public class RegionSeat extends Region{
    private int seatAmount;
    public int getSeatAmount(){
        return this.seatAmount;
    }

    public RegionSeat(String id, String name, int seatAmount){
        super(id, name);
        this.seatAmount = seatAmount;
    }
}
