
/**
 * @author Louisa Lin
 */
public class Party{
    private String id;
    private String partyName;
    private String leaderName;
 
    public Party(String id, String partyName, String leaderName){
     this.id = id;
     this.partyName = partyName;
     this.leaderName = leaderName;
    }
 
    public String getId(){
        return this.id;
    }
 
    public String getPartyName(){
        return this.partyName;
    }
 
    public String getLeaderName(){
        return this.leaderName;
    }
 
    @Override
    public String toString(){
        return "PartyId: " + this.id + ", PartyName: " + this.getPartyName() + ", LeaderName: " + this.getLeaderName();
    }
 
 }
 