import java.io.IOException;
import java.util.*;

/**
 * @author Qiuning Liang
 * done with the help of a tutor
 */
public interface Loader {

	List<People> loadPeoplesFromFile(String Filename) throws IOException;

}
