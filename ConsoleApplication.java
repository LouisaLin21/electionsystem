import java.io.IOException;
import java.util.*;

/**
 * This is the application class for the program.
 * @author  Louisa Lin
 */
public class ConsoleApplication {
    public static void main(String[]args) throws IOException, InterruptedException {
        // Load the files
        ArrayList<CastVote> voting= new ArrayList<>();
        // create your people arrays and put them into arraylist(castVote)
        List<CastVote> castVotes = new DataLoader().loadCastVotesFromFile("CastVote.txt");
        // Calculate the result by two methods
        while (true) {
            System.out.println("Please select the method to calculate vote count.");
            System.out.println("1: CalculationByparty");
            System.out.println("2: CalculationByRegion");
            System.out.println("99: Exit");
            Scanner scanner = new Scanner(System.in);
            if (scanner.hasNextInt()) {
                int method = scanner.nextInt();
                String voteResult = "";
                if (method == 1) {
                    CalculationByParty calculationByP = new CalculationByParty();
                    voteResult = calculationByP.voteCount(castVotes);
                } else if (method == 2) {
                    CalculationByRegion calculationByRegion = new CalculationByRegion();
                    voteResult = calculationByRegion.voteCount(castVotes);
                } else if (method == 99) {
                    scanner.close();
                    System.out.println("Exit The Election System!");
                    break;
                } else {
                    System.out.println("Please enter the correct number!");
                }
                System.out.println(voteResult);
            } else {
                System.out.println("Please enter the correct number!");
            }
            System.out.println();
        }
    }
}
